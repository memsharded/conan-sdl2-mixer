# Hello World for SDL2_mixer conan package

This repo is intented to test that the conan package SDL2_mixer/2.0.1@jmmut/testing
works fine.

## Using
To test this make sure you have the dependencies, and use this commands:

### Linux
```
git clone https://bitbucket.org/mutcoll/conan-sdl2-mixer
cd conan-sdl2-mixer
conan test_package --build=missing
```

and then you should hear a piano note for about 1.5 seconds.

## Dependencies

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)

